<?php

use App\Models\Employee;
use App\Models\Office;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        DB::table('offices')->truncate();
        DB::table('employees')->truncate();

        factory(Office::class, 10)->create()->each(function ($office) {
            factory(Employee::class, 10)->create(['office_id' => $office->id]);
        });
    }
}
