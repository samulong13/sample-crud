<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * May dalawang klase ng Route one for api then web 
 * ito yung parang view sir
 * pwedeng maglagay ng prefix sa web and api
 * like this project sir naglagay ako ng prefix na basiccrud
 * nasa app/Providers/RouteServiceProvider.php ung details
 * 
 * 
 */

// Route::get('/', function () {
//     return view('index');
// });

    /**
     * pwede maglagay ng {any} "/any" para sabihin na if ever na 
     * anong url ang puntahan after http://127.0.0.1:8000/basiccrud/
     * ee ung index.blade.php pa rin irerender niya
     * 
     * eg. http://127.0.0.1:8000/basiccrud/users
     * 
     * pwdeng maglagay ng  ->where('any', '.*')  sa huli para iextend yung url na irerender ay index.blade.php
     * para kahit anong ilagay sa url after http://127.0.0.1:8000/basiccrud/ irerender parin index.blade.php
     * 
     * eg. http://127.0.0.1:8000/basiccrud/users/profile
     *     http://127.0.0.1:8000/basiccrud/users/edit
     */

Route::get('/', function () {
    /**
     * itong view ee nkalink sa resources index.blade.php
     * index name ng view
     */
    return view('index');
});
