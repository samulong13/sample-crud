<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


/**
 * Route::apiResource ang gagamitin shorcut lang to dito
 * 
 *   yung http://127.0.0.1:8000/api/employee ihahandle to ng EmployeeController
 * 
 *   Route::get('employee', 'EmployeeController@index');
 *   Route::post('employee', 'EmployeeController@store');
 *   Route::put('employee/{employee}', 'EmployeeController@update');
 *   Route::delete('employee/{employee}', 'EmployeeController@destroy');
 *      
 *   pwede rin malaman lahat ng endpoint using "php artisan route:list"
 **/

Route::apiResource('employee','EmployeeController');
Route::apiResource('office','OfficeController');
