Requirements
    composer
    node js
    xammp


Download and install xammp

Download and install node js
Open cmd and type npm to check if recognize

Download composer-setup.exe from https://getcomposer.org/doc/00-intro.md#installation-windows
Install composer-setup.exe then add the path to environment variable C:\Users\{{name of user}}\AppData\Roaming\Composer\vendor\bin
Open cmd and type composer to check if recognize

Install laravel using command "composer global require laravel/installer"
Create laravel project "laravel new CRUD" or "composer create-project --prefer-dist laravel/laravel CRUD"

Install laravel/ui using command "composer require laravel/ui"

run "npm install" to install npm dependencies
run "npm run dev" to test if working

create database "crud" using sqlyog or phpmyadmin 

edit .env file then enter database name in the attribute DB_DATABASE=crud

test the laravel project using command php artisan serve

if laravel development server started open browser then type http://127.0.0.1:8000

install spatie/laravel-fractal "composer require spatie/laravel-fractal"
importante to para makapag create ng transformer
ito yung ginagamit ko
usefull to append tapos change attributes sa request
eg. sa employee office_id lang nakalagay
    pwede tayong mag append ng office_name para malamang yung office name ng employee then append natin yun sa response


install vue "npm install vue"
the run npm run watch

edit routes/web.php

create new vue file sa resources/views/pages/App.vue

edit resources/views/index.blade.php
    add cdn files

edit resources/js/app.js



create model using 
    "php artisan make:model Models/Employee -m"
    yung -m para mag create siya ng migration file for table sa database
    and snake case ang formate ng table eg. make:model EmployeeProfile mag creacreate siya ng employee_profiles na table
    then edit migration file created nasa database/migrations/

    edit model created

    create controller for employee and office
    "php artisan make:controller EmployeeController --api"
    "php artisan make:controller OfficeController --api"
    yung --api para sabihin na for api resource lang

    create transformer for employee and office
    "php artisan make:controller OfficeController --api"
    edit yung EmployeeController and OfficeController nasa app/Http/Controllers

    magcreate na ng url for the model using the controller created sa routes/api.php

    