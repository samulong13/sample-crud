<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    // list the fields na kailangan lang ifill
    protected $fillable = [
        'name',
    ];

    protected $hidden = ['created_at', 'updated_at'];
    // Add relationship to the model
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
