<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    // list the fields na kailangan lang ifill
    protected $fillable = [
        'firstname',
        'lastname',
        'status',
        'email',
        'office_id'
    ];

    // ito para ihide yung mga attribute na di kailangan
    protected $hidden = ['created_at', 'updated_at'];

    // Add relationship to the model
    // carefull lang sa naming case sensitive the follow pattern
    // same dapat sa name of model then lower case ung una
    //https://laravel.com/docs/7.x/eloquent-relationships
    public function office()
    {
        return $this->belongsTo(Office::class);
    }
}
