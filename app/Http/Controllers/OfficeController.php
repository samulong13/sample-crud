<?php

namespace App\Http\Controllers;

use App\Models\Office;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  response()->json(Office::all(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            request()->all(),
            [
                'name' => 'required',
            ]
        );

        /**
         * check kapag di na met ng request ung rules then mag response tayo ng error
         */
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        /**
         * if valid pwede na nating store sa database
         * using DB class or Model
         * 
         * mas okay kung gagamit ng try catch then DB::transaction
         * para if incase may error mahahandle pa natin
         */


        //start transaction
        DB::beginTransaction();
        try {

            $office = new Office;

            $office->name = $request->name;
            $office->save();

            DB::commit();

            return response()->json($office, 201);
        } catch (Exception $e) {
            // rollback if may error then return error message
            DB::rollBack();
            return response()->json(['error' => 'Unable to store office'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office =  Office::find($id);
        if (!$office)
            return response()->json(['error' => 'Office not found'], 400);

        return $office;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            request()->all(),
            [
                'name' => 'required',
            ]
        );

        /**
         * check kapag di na met ng request ung rules then mag response tayo ng error
         */
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        //start transaction
        DB::beginTransaction();
        try {

            $office =  Office::find($id);

            $office->name = isset($request->name) ? $request->name : $office->name;
            $office->save();

            DB::commit();
          
            return response()->json($office, 200);
        } catch (Exception $e) {
            // rollback if may error then return error message
            DB::rollBack();
            return response()->json(['error' => 'Unable to update office'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $office =  Office::find($id);
        if (!$office)
            return response()->json(['error' => 'Office not found'], 400);

        DB::beginTransaction();
        try {

            $office->delete();

            DB::commit();

            return response()->json(['success' => 'Office Successfully Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Unable to update office'], 400);
        }
    }
}
