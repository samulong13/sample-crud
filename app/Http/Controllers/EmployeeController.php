<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Transformers\EmployeeTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Fractalistic\ArraySerializer;
use Spatie\Fractalistic\Fractal;

/**
 * iimport yung model na kailangan
 */


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /***
         * madaming parang para magquery sa database
         * 
         * using DB from Illuminate\Support\Facades\DB
         * https://laravel.com/docs/7.x/database
         * DB::select('select * from employees')
         * 
         * or direkta sa model 
         * https://laravel.com/docs/7.x/eloquent
         * Employee::all();
         * 
         * then transform using the transformer created para maappend ung office name
         */
        $data = Fractal::create(
            Employee::all(),
            new EmployeeTransformer(),
            new ArraySerializer()
        );
        return  response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * ganito mag validate ng request
         * https://laravel.com/docs/7.x/validation
         */
        $validator = Validator::make(
            request()->all(),
            [
                'firstname' => 'required',
                'lastname' => 'required',
                'status' => 'required',
                'email' => 'required|email',
                'office_id' => 'required|integer'
            ]
        );

        /**
         * check kapag di na met ng request ung rules then mag response tayo ng error
         */
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        /**
         * if valid pwede na nating store sa database
         * using DB class or Model
         * 
         * mas okay kung gagamit ng try catch then DB::transaction
         * para if incase may error mahahandle pa natin
         */


        //start transaction
        DB::beginTransaction();
        try {

            $employee = new Employee;

            $employee->firstname = $request->firstname;
            $employee->lastname = $request->lastname;
            $employee->email = $request->email;
            $employee->status = $request->status;
            $employee->office_id = $request->office_id;
            $employee->save();

            DB::commit();
            /** commit transaction pag okay then return created response code
             *  pwede ring mag return ulit ng collection
             *  using Employee::all
             *  or return $this->index();
             * */
            $data = Fractal::create(
                $employee,
                new EmployeeTransformer(),
                new ArraySerializer()
            );
            return response()->json($data, 201);
        } catch (Exception $e) {
            // rollback if may error then return error message
            DB::rollBack();
            return response()->json(['error' => 'Unable to store employee'], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /**
         * ito yung showing ng single model
         * $id is yung id nanakaappend sa url http://127.0.0.1:8000/api/employee/9 using get method
         * pweding using DB::class or eloquent model ang pag find
         */
        $employee =  Employee::find($id);
        if (!$employee)
            return response()->json(['error' => 'Employee not found'], 400);
        $data = Fractal::create(
            $employee,
            new EmployeeTransformer(),
            new ArraySerializer()
        );
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * validate request
         * tapos pag okay 
         * hanapin ung model with id na nasa paramemter
         * tapos check isa isa attribute if may nagbago then save
         */
        $validator = Validator::make(
            request()->all(),
            [
                'firstname' => 'required',
                'lastname' => 'required',
                'status' => 'required',
                'email' => 'required|email',
                'office_id' => 'required|integer'
            ]
        );

        /**
         * check kapag di na met ng request ung rules then mag response tayo ng error
         */
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        /**
         * if valid pwede na nating store sa database
         * using DB class or Model
         * 
         * mas okay kung gagamit ng try catch then DB::transaction
         * para if incase may error mahahandle pa natin
         */


        //start transaction
        DB::beginTransaction();
        try {

            $employee =  Employee::find($id);

            $employee->firstname = isset($request->firstname) ? $request->firstname : $employee->firstname;
            $employee->lastname = isset($request->lastname) ? $request->lastname : $employee->lastname;
            $employee->email = isset($request->email) ? $request->email : $employee->email;
            $employee->status = isset($request->status) ? $request->status : $employee->status;
            $employee->office_id = isset($request->office_id) ? $request->office_id : $employee->office_id;
            $employee->save();

            DB::commit();
            /** commit transaction pag okay then return created response code
             *  pwede ring mag return ulit ng collection
             *  using Employee::all
             *  or return $this->index();
             * */
            $data = Fractal::create(
                $employee,
                new EmployeeTransformer(),
                new ArraySerializer()
            );
            return response()->json($data, 200);
        } catch (Exception $e) {
            // rollback if may error then return error message
            DB::rollBack();
            return response()->json(['error' => 'Unable to update employee'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**
         * find ung model with id na nasa parameter
         * check if exist then delete
         */
        $employee =  Employee::find($id);
        if (!$employee)
            return response()->json(['error' => 'Employee not found'], 400);

        DB::beginTransaction();
        try {

            $employee->delete();

            DB::commit();

            return response()->json(['success' => 'Employee Successfully Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Unable to update employee'], 400);
        }
    }
}
